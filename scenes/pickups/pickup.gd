extends RigidBody

const SOUND_EMITTER = preload("res://scenes/SoundEmitter/SoundEmitter.tscn")
const SPEED_DIFF = 2.0
const UPDATE_TIME = 0.3

var update_timer = 0.0
var last_speed = 0.0
var sound_delay = 0.0
var last_speed_diff = 0.0
var speed_diff = 0.0

var acceleration = 0.0

func _ready():
	set_physics_process(true)

func _physics_process(dt):
	if sound_delay <= 0.0:
		update_timer += dt
		if update_timer >= UPDATE_TIME:
			update_timer = 0.0
			var speed = self.linear_velocity.length()
			last_speed_diff = speed_diff
			speed_diff = speed-last_speed
			acceleration = last_speed_diff - speed_diff
			print(acceleration)
			if acceleration >= SPEED_DIFF:
				play_sound(acceleration)
				sound_delay = acceleration
	else:
		sound_delay -= dt

func play_sound(vol):
	print(vol)
	var emitter = SOUND_EMITTER.instance()
	emitter.radius = vol
	emitter.time = 0.3
	emitter.translation = self.global_transform.origin
	get_parent().add_child(emitter)
