extends Area

export var radius = 1.0
export var time = 1.0
var timer = 0.0

func _ready():
	self.translate(Vector3(0,0.5,0))
	if Game.player.close_to_danger:
		set_process(true)
	else:
		set_process(false)
		queue_free()

func _process(dt):
	timer += dt
	var val = lerp(0, radius, timer / time)
	$CollisionShape.scale = Vector3(val,0.2,val)
	if timer >= time:
		$CollisionShape.disabled = true
		set_process(false)
		queue_free()


func _on_SoundEmitter_body_entered(body):
	if body.is_in_group("Listener"):
		body.heard(self.global_transform.origin)
