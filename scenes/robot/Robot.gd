extends KinematicBody

enum {WORKING, SUCCEEDED, FAILED}
enum STATE {IDLE, PATROL, LOOK, SEARCH, CHASE, GRAB}
var state = STATE.IDLE

# global vars
export(NodePath) var nav_node_path
var nav : Navigation
var nav_path : Array = []
var nav_index := 0
var nav_dest : Vector3
var cur_dest : Vector3
var move_dir := Vector3(1,0,0)
var look_dir := Vector3(1,0,0)
var sees_player := false 
var vision_range := 8
var velocity := Vector3()
var speed := 0.0

# global consts
const RUN_SPEED := 10.0
const WALK_SPEED := 5.0
const MAX_ANGLE = deg2rad(65)
const GRAVITY = Vector3(0,-9.8,0)
const RED := Color("ff5050")
const BLUE := Color("50caff")
const YELLOW := Color("e9ff50")

# idle vars
var idle_pos : Vector3
var idle_look : Vector3

# patrol vars
export var out_and_back := false
export var path_point_wait := 0.0
var patrol_wait_timer = 0.0
var path : Array = []
var path_index := 0

# search vars
var search_pos : Vector3
var reached_search_pos := false
var search_timer := 0.0
var SEARCH_MIN_TIME = 5.0

# look vars
var see_time := 0.0
var total_see_time := 0.0
const MIN_LOOK_TIME := 0.5
const SEE_TIME_LIMIT := 0.7

# chase vars
var chase_time := 0.0
var last_seen_pos : Vector3
const MIN_CHASE_TIME = 6.0

# grab vars
const GRAB_DIST = 2.5

func _ready():
	nav = get_node(nav_node_path)

	look_dir = self.translation - self.transform.basis.z
	move_dir = look_dir
	idle_pos = self.translation
	idle_look = look_dir
	
	cur_dest = self.translation
	speed = WALK_SPEED
	
	var children = get_children()
	for c in children:
		if c is Spatial and c.name.begins_with("Path"):
			path.append(nav.get_closest_point(nav.to_local(c.global_transform.origin)))
			c.free()
	var im = nav.get_node("Draw")
	if im:
		im.clear()
		im.begin(Mesh.PRIMITIVE_LINE_STRIP, null)
		for x in path:
			im.add_vertex(x)
		im.end()
	
	vision_range = $SpotLight.spot_range
	
	set_process(true)
	set_physics_process(true)

func _process(dt):
	pass

func _physics_process(dt):
	var player_pos = nav.to_local(Game.player.global_transform.origin)
	match state:
		STATE.IDLE:
			$SpotLight.light_color = BLUE
			$Huh.visible = false
			if path.size() > 0:
				nav_path.clear()
				state = STATE.PATROL
			else:
				if nav_path.size() == 0:
					velocity = Vector3()
					look_dir = idle_look
				else:
					move_on_nav_path(dt)
					
		STATE.PATROL:
			$Huh.visible = false
			if nav_path.size() == 0:
				path_index += 1
				if path_index >= path.size():
					# todo: out and back paths
					path_index = 0
				var begin = self.translation
				var end = nav.get_closest_point(path[path_index])
				nav_path = Array(nav.get_simple_path(begin, end, true))
				nav_path.pop_front()
				
				var im = nav.get_node("Draw2")
				if im:
					im.clear()
					im.begin(Mesh.PRIMITIVE_LINE_STRIP, null)
					for x in nav_path:
						im.add_vertex(x)
					im.end()
				
				nav_index = 0
				if path_point_wait > 0.0:
					patrol_wait_timer = path_point_wait
					self.velocity = Vector3()
			if patrol_wait_timer > 0.0:
				patrol_wait_timer -= dt
			else:
				move_on_nav_path(dt)
		STATE.LOOK:
			$Huh.visible = true
			total_see_time += dt
			if sees_player:
				$SpotLight.light_color = RED
				look_dir = self.translation - (player_pos - self.translation).normalized()
			else:
				$SpotLight.light_color = YELLOW
			if see_time >= SEE_TIME_LIMIT:
				nav_path = []
				speed = RUN_SPEED
				state = STATE.CHASE
			if total_see_time >= MIN_LOOK_TIME and see_time <= 0.0:
				$SpotLight.light_color = BLUE
				state = STATE.IDLE
				nav_path = Array(nav.get_simple_path(self.translation, idle_pos))
		STATE.SEARCH:
			$Huh.visible = true
			search_timer -= dt
			if (search_timer <= 0.0 and reached_search_pos) or search_timer < -SEARCH_MIN_TIME:
				state = STATE.IDLE
				nav_path = Array(nav.get_simple_path(self.translation, idle_pos))
			if !reached_search_pos:
				move_on_nav_path(dt)
			if nav_path.empty():
				velocity = Vector3()
				reached_search_pos = true
		STATE.CHASE:
			$Huh.visible = false
			chase_time += dt
			if see_time > 0.0 or chase_time < MIN_CHASE_TIME:
				$SpotLight.light_color = YELLOW
				if sees_player:
					chase_time = 0.0
					$SpotLight.light_color = RED
				var player_dist_nav_end = 0.0
				if nav_path.size() > 0:
					player_dist_nav_end = nav_path[nav_path.size()-1].distance_to(last_seen_pos)
				if (nav_path.size() == 0 or player_dist_nav_end > 1):
					var begin = self.translation
					var end = last_seen_pos
					if begin != end:
						nav_path = Array(nav.get_simple_path(begin, end, true))
						nav_path.pop_front()
					else:
						velocity = Vector3()
				move_on_nav_path(dt)
				var dist_for_grab = Vector3(translation.x,0,translation.z).distance_to(Vector3(player_pos.x,0,player_pos.z))
				if dist_for_grab < GRAB_DIST:
					state = STATE.GRAB
			else:
				chase_time = 0.0
				state = STATE.IDLE
				nav_path = Array(nav.get_simple_path(self.translation, idle_pos))
		STATE.GRAB:
			$Huh.visible = false
			if $AnimationPlayer.current_animation != "Grab":
				$AnimationPlayer.play("Grab")
			state = STATE.IDLE
			nav_path = Array(nav.get_simple_path(self.translation, idle_pos))
	
	velocity += GRAVITY * dt
	velocity = move_and_slide(velocity, Vector3(0,1,0), true, 4, MAX_ANGLE, true)
	look_dir.y = self.translation.y
	var target_transform = self.transform.looking_at(look_dir, Vector3(0,1,0))
	var new_basis = Basis( Quat(self.transform.basis).slerp(target_transform.basis, dt*6) )
	self.transform = Transform(new_basis, self.transform.origin)
	
	# sensor stuff
	sees_player = false
	if Vector3(translation.x,0,translation.z).distance_to(Vector3(player_pos.x,0,player_pos.z)) <= 2:
		sees_player = true
	if !sees_player and $SpotLight.global_transform.origin.distance_to(Game.player.global_transform.origin) <= vision_range:
		var angle = (Game.player.global_transform.origin - $SpotLight.global_transform.origin).normalized()
		var dot = -angle.dot($SpotLight.global_transform.basis.z)
		if dot > 0 and (360-rad2deg(dot*PI*2))/2 <= $SpotLight.spot_angle:
			var space_state = get_world().direct_space_state
			var result = space_state.intersect_ray($SpotLight.global_transform.origin, Game.player.global_transform.origin + Vector3(0,0.5,0), [self])
			if !result.empty() and result.collider:
				if result.collider.is_in_group("Player"):
					sees_player = true
	
	if sees_player:
		last_seen_pos = player_pos
		$SpotLight.light_color = RED
		see_time = see_time+dt
		if state == STATE.CHASE:
			see_time = SEE_TIME_LIMIT
		if state == STATE.IDLE or state == STATE.PATROL or state == STATE.SEARCH:
			velocity = Vector3()
			state = STATE.LOOK
			total_see_time = 0.0
	else:
		see_time = clamp(see_time-dt, 0.0, INF)

func move_on_nav_path(dt):
	if nav_path.size() == 0:
		return
	cur_dest = nav_path[0]
	move_dir = (cur_dest-self.translation).normalized()
	look_dir = (self.translation - move_dir)
	velocity = move_dir * speed
	if self.translation.distance_to(cur_dest) <= speed*dt:
		nav_path.pop_front()

func heard(loc):
	if state == STATE.CHASE or state == STATE.GRAB:
		last_seen_pos = nav.to_local(loc)
	else:
		state = STATE.SEARCH
		search_pos = nav.get_closest_point(nav.to_local(loc))
		nav_path = Array(nav.get_simple_path(nav.get_closest_point(self.translation), nav.get_closest_point(search_pos)))
		print(nav_path)
		reached_search_pos = false
		search_timer = SEARCH_MIN_TIME

func reset():
	path_index = 0
	see_time = 0.0
	total_see_time = 0.0
	patrol_wait_timer = 0.0
	search_timer = 0.0
	chase_time = 0.0
	last_seen_pos = Vector3()
	search_pos = Vector3()
	reached_search_pos = false
	self.translation = idle_pos
	look_dir = idle_look
	move_dir = idle_look
	nav_path = []
	cur_dest = Vector3()
	state = STATE.IDLE
	speed = WALK_SPEED
	velocity = Vector3()
	$Huh.visible = false
	sees_player = false

func game_over():
	Game.game_over()
