extends Spatial

var title_timer := 1.0
var title_done = false
var movement_done = false
var look_done = false
var sprint_done = false

func _ready():
	set_process(true)

func _process(dt):
	title_timer -= dt
	if title_timer <= 0 and !title_done:
		title_done = true
		Game.hud.update_big_title("What's Inside", true)
	if title_timer <= -5 and !movement_done:
		movement_done = true
		Game.hud.update_tutorial_text("WASD/Left Stick:\n  Move", true, 1, 5, 0.3)
	if title_timer <= -12 and !look_done:
		look_done = true
		Game.hud.update_tutorial_text("Mouse/Right Stick:\n  Look", true, 1, 5, 0.3)
	if title_timer <= -19 and !sprint_done:
		sprint_done = true
		Game.hud.update_tutorial_text("Shift/RB/R1:\n  Sprint", true, 1, 5, 0.3)
		set_process(false)
		

func _on_keycard_area_entered(body):
	if body.is_in_group("Player"):
		var text = "E/B/Cross:\n  use or pick up" 
		Game.hud.update_tutorial_text(text, true, 0.5,5,0.3)
