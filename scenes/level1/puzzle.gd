extends Area

#var tiger = ["ti", "ige", "er"]
#var snake = ["sn", "nak", "ke"]
#var dragon = ["dr", "rag", "on"]

var keycard_found = false

var start = ["ti", "sn", "dr"]
var mid = ["ige", "nak", "rag"]
var end = ["er", "ke", "on"]

var order = [0,1,2]

var order_index = 0

var prompt := ""

var selection := -1

func _ready():
	randomize()
	order.shuffle()
	prompt = "[center]" + start[order[0]] + "-" + mid[order[1]] + "-" + end[order[2]] + "[/center]"
	set_process_input(true)

#func _process(dt):
#	pass

func _input(ev):
	if Input.is_action_just_pressed("use"):
		use()

func use():
	if order_index < 3:
		if selection == order[order_index]:
			order_index += 1
			if order_index != 3:
				set_green(selection)
		else:
			get_node("../AnimationPlayer").play("FailedPuzzle")
			order_index = 0
		if order_index == 3:
			selection = -1
			get_node("../AnimationPlayer").play("CompletePuzzle")
			Game.load_level(2)
			keycard_found = false

func set_green(index):
	match index:
		0: #tiger
			get_node("../ScreenLevel2A/ColoredLight").set_color(Color(0,1,0,1))
			get_node("../ScreenLevel2A/ColoredLight").turn_on()
		1: #snake
			get_node("../ScreenLevel2C/ColoredLight3").set_color(Color(0,1,0,1))
			get_node("../ScreenLevel2C/ColoredLight3").turn_on()
			pass
		2: #dragon
			get_node("../ScreenLevel2B/ColoredLight2").set_color(Color(0,1,0,1))
			get_node("../ScreenLevel2B/ColoredLight2").turn_on()
			pass

func _on_PuzzleMainArea_body_entered(body):
	if body.is_in_group("Player"):
		if Game.player.current_pickup and Game.player.current_pickup.is_in_group("Keycard"):
			var k : RigidBody = Game.player.current_pickup
			k.mode = RigidBody.MODE_STATIC
			k.remove_from_group("Pickup")
			Game.player.drop_pickup()
			Game.player.pickups_in_range.erase(get_path_to(k))
			k.transform.origin = Vector3(0.766, 1.146, -1.631)
			k.rotation = Vector3(deg2rad(30),0,0)
			get_node("../ViewportMain/Level1MainScreen").set_bg(Color(0.05,0.3,0.1,1.0), false)
			get_node("../ViewportMain/Level1MainScreen").set_text(prompt, 120)
			keycard_found = true


func _on_TigerArea_body_entered(body):
	if body.is_in_group("Player") and keycard_found:
		selection = 0


func _on_DragonArea_body_entered(body):
	if body.is_in_group("Player") and keycard_found:
		selection = 2


func _on_SnakeArea_body_entered(body):
	if body.is_in_group("Player") and keycard_found:
		selection = 1


func _on_TigerArea_body_exited(body):
	if body.is_in_group("Player") and selection == 0:
		selection = -1


func _on_DragonArea_body_exited(body):
	if body.is_in_group("Player") and selection == 2:
		selection = -1


func _on_SnakeArea_body_exited(body):
	if body.is_in_group("Player") and selection == 1:
		selection = -1