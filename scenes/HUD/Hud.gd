extends Container

onready var BigTitle = $SafeZone/BigTitle
onready var TutorialText = $SafeZone/TutorialText
onready var Black = $Black

var big_title_in := 0.0
var big_title_hold := 0.0
var big_title_out := 0.0
var big_title_timer := 0.0

var tutorial_text_in := 0.0
var tutorial_text_hold := 0.0
var tutorial_text_out := 0.0
var tutorial_text_timer := 0.0

var black_in := 0.0
var black_hold := 0.0
var black_out := 0.0
var black_timer := 0.0

func _ready():
	set_process(false)

func _process(dt):
	var big_title_max_time = big_title_in+big_title_hold+big_title_out
	var tutorial_text_max_time = tutorial_text_in+tutorial_text_hold+tutorial_text_out
	var black_max_time = black_in+black_hold+black_out
	
	if big_title_timer < big_title_max_time:
		big_title_timer = clamp(big_title_timer+dt, 0, big_title_max_time)
	if tutorial_text_timer < tutorial_text_max_time:
		tutorial_text_timer = clamp(tutorial_text_timer+dt, 0, tutorial_text_max_time)
	if black_timer < black_max_time:
		black_timer = clamp(black_timer+dt, 0, black_max_time)
	
	if big_title_timer > 0.0:
		if big_title_timer <= big_title_in:
			var alpha = big_title_timer / big_title_in
			BigTitle.self_modulate = Color(1,1,1, alpha)
		elif big_title_timer <= big_title_in+big_title_hold:
			BigTitle.self_modulate = Color(1,1,1,1)
		elif big_title_timer <= big_title_max_time:
			var alpha = 1-(big_title_timer-big_title_in-big_title_hold) / big_title_out
			BigTitle.self_modulate = Color(1,1,1, alpha)
		else:
			BigTitle.self_modulate = Color(1,1,1, 0)
		
	if tutorial_text_timer > 0.0:
		if tutorial_text_timer <= tutorial_text_in:
			var alpha = tutorial_text_timer / tutorial_text_in
			TutorialText.self_modulate = Color(1,1,1, alpha)
		elif tutorial_text_timer <= tutorial_text_in+tutorial_text_hold:
			TutorialText.self_modulate = Color(1,1,1,1)
		elif tutorial_text_timer <= tutorial_text_max_time:
			var alpha = 1-(tutorial_text_timer-tutorial_text_in-tutorial_text_hold) / tutorial_text_out
			TutorialText.self_modulate = Color(1,1,1, alpha)
		else:
			TutorialText.self_modulate = Color(1,1,1, 0)
	
	if black_timer > 0.0:
		if black_timer <= black_in:
			var alpha = black_timer / black_in
			Black.self_modulate = Color(1,1,1, alpha)
		elif black_timer <= black_in+black_hold:
			Black.self_modulate = Color(1,1,1,1)
		elif black_timer <= black_max_time:
			var alpha = 1-(black_timer-black_in-black_hold) / black_out
			Black.self_modulate = Color(1,1,1, alpha)
		else:
			Black.self_modulate = Color(1,1,1, 0)
		
	
	if big_title_timer >= big_title_max_time and tutorial_text_timer >= tutorial_text_max_time and black_timer >= black_max_time:
		BigTitle.self_modulate = Color(1,1,1,0)
		TutorialText.self_modulate = Color(1,1,1,0)
		Black.self_modulate = Color(1,1,1,0)
		set_process(false)

func update_big_title(text, show):
	BigTitle.set_use_bbcode(true)
	text = "[center]"+text+"[/center]"
	BigTitle.set_bbcode(text)
	BigTitle.self_modulate = Color(1,1,1,0)
	if show:
		show_big_title(1,1,2)

func show_big_title(fadein, hold, fadeout):
	big_title_in = fadein
	big_title_hold = hold
	big_title_out = fadeout
	big_title_timer = 0.0
	set_process(true)

func update_tutorial_text(text, show, fadein, hold, fadeout):
	TutorialText.set_bbcode(text)
	TutorialText.self_modulate = Color(1,1,1,0)
	if show:
		show_tutorial_text(fadein, hold, fadeout)

func show_tutorial_text(fadein, hold, fadeout):
	tutorial_text_in = fadein
	tutorial_text_hold = hold
	tutorial_text_out = fadeout
	tutorial_text_timer = 0.0
	set_process(true)

func fade_to_black(fadein, hold, fadeout):
	black_in = fadein
	black_hold = hold
	black_out = fadeout
	black_timer = 0.0
	set_process(true)




