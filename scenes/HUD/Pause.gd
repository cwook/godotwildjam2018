extends Panel

# Declare member variables here. Examples:
# var a = 2
# var b = "text"

# Called when the node enters the scene tree for the first time.
func _ready():
	set_process_input(true)

func _input(ev):
	if Input.is_action_just_pressed("pause"):
		get_tree().paused = !get_tree().paused
		if get_tree().paused:
			Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)
			self.visible = true
			self.mouse_filter = Control.MOUSE_FILTER_IGNORE
		else:
			Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)
			self.visible = false
			self.mouse_filter = Control.MOUSE_FILTER_STOP

func _on_Reset_button_up():
	get_tree().paused = false
	self.visible = false
	self.mouse_filter = Control.MOUSE_FILTER_STOP
	Game.game_over()


func _on_Quit_button_up():
	get_tree().quit()


func _on_OptionResolution_item_selected(ID):
	if ID == 4:
		get_tree().set_screen_stretch(SceneTree.STRETCH_MODE_2D, SceneTree.STRETCH_ASPECT_KEEP, Vector2(1280,720),1)
		return
	match ID:
		0:
			OS.set_window_size(Vector2(1280,720))
			get_tree().set_screen_stretch(SceneTree.STRETCH_MODE_VIEWPORT, SceneTree.STRETCH_ASPECT_KEEP, Vector2(1280,720), 1)
		1:
			OS.set_window_size(Vector2(1366,768))
			get_tree().set_screen_stretch(SceneTree.STRETCH_MODE_VIEWPORT, SceneTree.STRETCH_ASPECT_KEEP, Vector2(1366,768), 1)
		2:
			OS.set_window_size(Vector2(1600,900))
			get_tree().set_screen_stretch(SceneTree.STRETCH_MODE_VIEWPORT, SceneTree.STRETCH_ASPECT_KEEP, Vector2(1600,900), 1)
		3:
			OS.set_window_size(Vector2(1920,1080))
			get_tree().set_screen_stretch(SceneTree.STRETCH_MODE_VIEWPORT, SceneTree.STRETCH_ASPECT_KEEP, Vector2(1920,1080), 1)
	
	OS.set_window_position(OS.get_screen_size()/2 - OS.get_real_window_size()/2)


func _on_OptionVsync_button_up():
	var vsync = OS.vsync_enabled
	OS.set_use_vsync(!vsync)
	if OS.vsync_enabled:
		$BoxContainer/GridContainer/OptionVsync.text = "ON"
	else:
		$BoxContainer/GridContainer/OptionVsync.text = "OFF"


func _on_OptionFullscreen_item_selected(ID):
	match ID:
		0:
			#windowed
			OS.set_borderless_window(false)
			OS.set_window_fullscreen(false)
			_on_OptionResolution_item_selected($BoxContainer/GridContainer/OptionResolution.get_selected_id())
		1:
			#fullscreen
			OS.set_window_fullscreen(true)
			$BoxContainer/GridContainer/OptionResolution.disabled = false
			_on_OptionResolution_item_selected($BoxContainer/GridContainer/OptionResolution.get_selected_id())
		2:
			#borderless fullscreen
			OS.set_window_size(OS.get_screen_size())
			OS.set_window_fullscreen(true)
			OS.set_borderless_window(true)
			OS.set_window_fullscreen(false)
			OS.set_borderless_window(true)
			OS.set_window_position(Vector2(0,0))
			get_tree().set_screen_stretch(SceneTree.STRETCH_MODE_2D, SceneTree.STRETCH_ASPECT_KEEP, OS.get_window_size(), 1)
			$BoxContainer/GridContainer/OptionResolution.disabled = true


func _on_Resume_button_up():
		get_tree().paused = !get_tree().paused
		if get_tree().paused:
			Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)
			self.visible = true
			self.mouse_filter = Control.MOUSE_FILTER_IGNORE
		else:
			Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)
			self.visible = false
			self.mouse_filter = Control.MOUSE_FILTER_STOP


func _on_OptionMSAA_item_selected(ID):
	print(int(pow(2,float(ID))))
	match ID:
		0:
			get_tree().root.get_viewport().set_msaa(Viewport.MSAA_DISABLED)
		1:
			get_tree().root.get_viewport().set_msaa(Viewport.MSAA_2X)
		2:
			get_tree().root.get_viewport().set_msaa(Viewport.MSAA_4X)
		3:
			get_tree().root.get_viewport().set_msaa(Viewport.MSAA_8X)
		4:
			get_tree().root.get_viewport().set_msaa(Viewport.MSAA_16X)


func _on_OptionFiltering_item_selected(ID):
	print(int(pow(2,float(ID))))
	ProjectSettings.set_setting("rendering/quality/filters/anisotropic_filter_level", int(pow(2,float(ID))))


func _on_OptionSSAO_toggled(button_pressed):
	if button_pressed:
		get_tree().root.get_viewport().world.fallback_environment.set_ssao_enabled(true)
		$BoxContainer/GridContainer/OptionSSAO.text = "ON"
	else:
		get_tree().root.get_viewport().world.fallback_environment.set_ssao_enabled(false)
		$BoxContainer/GridContainer/OptionSSAO.text = "OFF"


func _on_OptionShadows_item_selected(ID):
	var power := int(10+ID)
	VisualServer.viewport_set_shadow_atlas_size(get_tree().root.get_viewport_rid(), int(pow(2,power)))
	get_tree().root.get_node("Main/DirectionalLight").shadow_enabled = true
	if ID == 0:
		get_tree().root.get_node("Main/DirectionalLight").shadow_enabled = false
