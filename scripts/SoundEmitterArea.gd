extends Area

export var collision_shape_path : NodePath

# Called when the node enters the scene tree for the first time.
func _ready():
	var col = get_node(collision_shape_path)
	var col_dupe : CollisionShape = col.duplicate()
	var shape_dupe = col.shape.duplicate(true)
	#col_dupe.translate(Vector3(0,0.2,0))
	#col_dupe.disabled = false
	#create_shape_owner(col_dupe)
	print(shape_dupe)
	shape_owner_get_owner(0).translate(Vector3(0,1,0))
	shape_owner_add_shape(0, shape_dupe)
	shape_owner_set_disabled(0, false)
	print(shape_owner_get_shape(0,0))
	add_child(col_dupe)

func _on_Area_body_entered(body):
	print("nana")
	if body.is_in_group("Pickup"):
		print("pickip")