extends KinematicBody

var cam : Camera

var input := Vector2()

var velocity := Vector3()

var desired_face_direction := Vector3(1,0,1)
var look_speed := 0.1
var speed : float
var sprint_timer := 0.0
var throw_timer := 0.0

var jumping = false
var close_to_danger = false
var emit_radius_norm = 10
var emit_radius_creep = 3
var emit_radius_sprint = 18

const SPRINT_DURATION := 5.0
const CREEP_SPEED := 2.5
const WALK_SPEED := 5.5
const SPRINT_SPEED := 8.0
const GRAVITY := Vector3(0, -9.8, 0)
const MAX_ANGLE = deg2rad(65)
const JUMP_POW := 6.0
const THROW_POW := 15.0
const THROW_MAX_TIME := 0.8

const SOUND_EMITTER = preload("res://scenes/SoundEmitter/SoundEmitter.tscn")

var last_respawn : Vector3

var pickups_in_range := {}

var current_pickup : RigidBody = null
var pickup_is_static = false

func _ready():
	last_respawn = Game.player_spawn
	speed = WALK_SPEED
	sprint_timer = SPRINT_DURATION
	cam = get_tree().root.get_camera()
	set_process(true)
	set_physics_process(true)

func _process(dt):
	close_to_danger = false
	var robots = get_tree().get_nodes_in_group("Robot")
	for r in robots:
		if r.global_transform.origin.distance_to(Game.player.global_transform.origin) <= 4*emit_radius_sprint:
			close_to_danger = true
			break
	
	input = Vector2(Input.get_action_strength("left") - Input.get_action_strength("right"), Input.get_action_strength("forward") - Input.get_action_strength("back"))
	if Input.is_action_pressed("sprint"):
		sprint_timer -= dt
	else:
		sprint_timer += dt
	
	sprint_timer = clamp(sprint_timer, 0.0, SPRINT_DURATION)
	
	var desired_speed = WALK_SPEED
	if Input.is_action_pressed("creep"):
		$Footsteps.unit_db = -8
		desired_speed = CREEP_SPEED
	elif Input.is_action_pressed("sprint") and sprint_timer > 0:
		if Input.is_action_just_pressed("sprint") and velocity.length() > 1 and (is_on_floor() or ($GroundRaycast.is_colliding() and $GroundRaycast.get_collision_normal().angle_to(Vector3(0,1,0)) <= MAX_ANGLE)):
			_emit_sound()
		$Footsteps.unit_db = 0
		desired_speed = SPRINT_SPEED
		$AnimationPlayer.playback_speed = 1.7
	else:
		$Footsteps.unit_db = -6
		$AnimationPlayer.playback_speed = 1
	speed = lerp(speed, desired_speed, dt*12)
	
	if Input.is_action_just_pressed("jump") and (is_on_floor() or ($GroundRaycast.is_colliding() and $GroundRaycast.get_collision_normal().angle_to(Vector3(0,1,0)) <= MAX_ANGLE)):
		jumping = true
		$AnimationPlayer.play("Jump")
		_emit_sound()
		
		velocity.y = JUMP_POW
	
	if is_on_ceiling() and velocity.y > 0:
		velocity.y *= -1
	
	if Input.is_action_pressed("use"):
		throw_timer = clamp(throw_timer+dt, 0.0, THROW_MAX_TIME)
	if Input.is_action_just_released("use"):
		if pickups_in_range.size() > 0:
			var closest = 9999999
			var to_pickup = null
			for p in pickups_in_range:
				if pickups_in_range[p] == current_pickup:
					continue
				var dir = (pickups_in_range[p].translation-self.translation)
				dir.y = 0
				dir = dir.normalized()
				var angle = abs(atan2(dir.x,dir.z) - self.rotation.y)
				if angle < closest:
					closest = angle
					to_pickup = pickups_in_range[p]
			if to_pickup != null:
				set_pickup(to_pickup)
			else:
				drop_pickup()
		elif current_pickup:
			drop_pickup()
		throw_timer = 0.0

func _physics_process(dt):
	var forward = -cam.global_transform.basis.z
	forward.y = 0
	forward = forward.normalized()
	var left = forward.rotated(Vector3(0,1,0), PI/2)
	if input.length() > 0:
		var vel : Vector3 = input.x * left + input.y * forward
		vel = vel.normalized() * speed
		velocity.x = vel.x
		velocity.z = vel.z
		if Vector3(vel.x,0,vel.y).length() > 0:
			desired_face_direction = self.translation - Vector3(vel.x,0,vel.z)
	else:
		velocity.x = 0
		velocity.z = 0
	
	velocity += GRAVITY * dt
	move_and_slide(velocity, Vector3(0,1,0), false, 4, MAX_ANGLE, true)
	if is_on_floor():
		if jumping:
			$AnimationPlayer.play("Land")
			_emit_sound()
		jumping = false
		velocity.y = 0
		if velocity.length() < 1:
			$Footsteps.stop()
			if Input.is_action_pressed("creep"):
				if $AnimationPlayer.current_animation != "Creep":
					$AnimationPlayer.play("Creep")
			else:
				if $AnimationPlayer.current_animation != "Idle":
					$AnimationPlayer.play("Idle")
		else:
			
			if !$Footsteps.playing:
				$Footsteps.play($Footsteps.get_playback_position())
			if Input.is_action_pressed("creep"):
				if $AnimationPlayer.current_animation != "CreepWalk":
					$AnimationPlayer.play("CreepWalk")
			else:
				if $AnimationPlayer.current_animation != "Walk":
					$AnimationPlayer.play("Walk")
	else:
		if !jumping and $GroundRaycast.is_colliding() and $GroundRaycast.get_collision_normal().angle_to(Vector3(0,1,0)) <= MAX_ANGLE:
			move_and_collide($GroundRaycast.cast_to*1.2)
		$Footsteps.stop()
	desired_face_direction.y = self.translation.y
	
	var rotTransform = self.transform.looking_at(desired_face_direction, Vector3(0,1,0))  #Transform(Basis(desired_face_direction), self.transform.origin)
	var thisRotation = Quat(self.transform.basis).slerp(rotTransform.basis,look_speed)
	set_transform(Transform(thisRotation,transform.origin))
	

func drop_pickup():
	self.remove_child(current_pickup)
	get_parent().add_child(current_pickup)
	current_pickup.remove_collision_exception_with(self)
	var throw_dir = (translation - desired_face_direction).normalized()
	var throw_pow = lerp(0, THROW_POW, throw_timer / THROW_MAX_TIME)
	if throw_timer < 0.25:
		throw_pow = 0.01
		current_pickup.translation = self.translation + Vector3(0,0.3,0) + throw_dir * 0.2
	else:
		current_pickup.translation = self.translation + Vector3(0,0.9,0) + throw_dir * 1
	current_pickup.sleeping = false
	throw_dir.y = 0.5
	throw_dir = throw_dir.normalized()
	if !pickup_is_static:
		current_pickup.set_mode(RigidBody.MODE_RIGID)
		current_pickup.apply_central_impulse(throw_dir * throw_pow * current_pickup.mass)
	current_pickup = null

func set_pickup(p):
	if current_pickup:
		drop_pickup()
	p.add_collision_exception_with(self)
	current_pickup = p
	current_pickup.get_parent().remove_child(current_pickup)
	self.add_child(current_pickup)
	if current_pickup.mode == RigidBody.MODE_STATIC:
		pickup_is_static = true
	else:
		pickup_is_static = false
	current_pickup.set_mode(RigidBody.MODE_STATIC)
	p.translation = Vector3(0,1,0.3)

func _on_PickupArea_body_entered(body):
	if body.is_in_group("Pickup"):
		self.add_collision_exception_with(body)
		pickups_in_range[get_path_to(body)] = body

func _on_PickupArea_body_exited(body):
	pickups_in_range.erase(get_path_to(body))

func _emit_sound():
	var emitter = SOUND_EMITTER.instance()
	if Input.is_action_pressed("creep"):
		emitter.radius = emit_radius_creep
		emitter.time = 0.5
	elif Input.is_action_pressed("sprint"):
		emitter.radius = emit_radius_sprint
		emitter.time = 0.5
	else:
		emitter.radius = emit_radius_norm 
		emitter.time = 0.5
	emitter.translation = self.global_transform.origin
	get_parent().add_child(emitter)
