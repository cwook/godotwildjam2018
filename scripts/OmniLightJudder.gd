extends OmniLight

export(Vector3) var max_translation = Vector3(0.1,0.3,0.1)
export(float) var max_range = 7
export(float) var hue_shift = 0.05
export(float) var max_energy = 0.2
export(float) var min_change_time = 0.01
export(float) var max_change_time = 0.1

var base_translation : Vector3
var base_range : float
var base_color : Color
var base_energy : float

var desired_translation : Vector3
var desired_range : float
var desired_color : Color
var desired_energy : float

var change_time : float
var new_change_time: float

func _ready():
	base_translation = self.translation
	base_range = self.omni_range
	base_color = self.light_color
	base_energy = self.light_energy
	change_time = rand_range(min_change_time, max_change_time)
	new_change_time = change_time
	set_process(true)

func _process(dt):
	change_time -= dt
	
	if change_time <= 0:
		change_time = rand_range(min_change_time, max_change_time)
		new_change_time = change_time
		desired_translation = base_translation + Vector3(rand_range(-max_translation.x, max_translation.x), rand_range(-max_translation.y, max_translation.y), rand_range(-max_translation.z, max_translation.z))
		desired_range = base_range + rand_range(0, max_range)
		desired_color = Color.from_hsv(base_color.h + rand_range(-hue_shift, hue_shift), base_color.s, base_color.v)
		desired_energy = base_energy + rand_range(-max_energy, max_energy)
	
	translation = translation.linear_interpolate(desired_translation, new_change_time-change_time)
	self.omni_range = lerp(self.omni_range, desired_range, new_change_time-change_time)
	self.light_color = self.light_color.linear_interpolate(desired_color, new_change_time-change_time)
	self.light_energy = lerp(self.light_energy, desired_energy, new_change_time-change_time)
















