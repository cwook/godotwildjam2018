extends Node

const Player = preload("res://scripts/Player.gd")
const Hud = preload("res://scenes/HUD/Hud.gd")
const Cam = preload("res://scripts/Camera.gd")

var player : Player
var hud : Hud
var cam : Cam
var level1
var level2
var level3
var level4

var reset_timer = -INF

var current_level

var main

var start_level := 1
var player_spawn := Vector3()

func _ready():
	Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)
	
	main = get_tree().root.get_node("Main")
	
	hud = get_tree().get_nodes_in_group("Hud")[0]
	
	player = get_tree().get_nodes_in_group("Player")[0]
	cam = get_tree().get_nodes_in_group("Cam")[0]
	if !player:
		print("No Player!")
	if !cam:
		print("No cam!")
	
	load_level(start_level)
	
	player.translation = player_spawn
	cam.translation = player_spawn
	
	yield(get_tree(), "idle_frame")
	yield(get_tree(), "idle_frame")
	hud.fade_to_black(0,0.3,0.5)
	
	set_process(true)

func _process(dt):
	var fps = 0
	if dt != 0:
		fps = 1.0 / dt
	var title = "What's Inside [" + String(fps) + "]"
	OS.set_window_title(title)
	if reset_timer != -INF:
		if reset_timer <= 0.0:
			reset_timer = -INF
			reset_level()
		reset_timer -= dt

# fixes "sticky" actions on alt-tab
#func _notification(what):
#	if what == MainLoop.NOTIFICATION_WM_FOCUS_OUT:
#		for setting in ProjectSettings.get_property_list():
#			if setting.name.substr(0,6) == "input/":
#				Input.action_release(setting.name.substr(6, setting.name.length()-6))

func load_level(index):
	var dlight : DirectionalLight
	if current_level:
		dlight = current_level.get_node("DirectionalLight")
	match index:
		1:
			var levelResource = ResourceLoader.load("res://scenes/level1/level1.tscn")
			level1 = levelResource.instance()
			current_level = level1
			main.add_child(level1)
			player_spawn = level1.get_node("PlayerSpawn").global_transform.origin
		2:
			var levelResource = ResourceLoader.load("res://scenes/level2/level2.tscn")
			level2 = levelResource.instance()
			main.add_child(level2)
			current_level = level2
			player_spawn = level2.get_node("PlayerSpawn").global_transform.origin
		3:
			var levelResource = ResourceLoader.load("res://scenes/level3/level3.tscn")
			level3 = levelResource.instance()
			main.add_child(level3)
			current_level = level3
			player_spawn = level3.get_node("PlayerSpawn").global_transform.origin
		4:
			ResourceLoader.set_abort_on_missing_resources(false)
			var levelResource = ResourceLoader.load("res://scenes/level4/level4.tscn")
			level4 = levelResource.instance()
			main.add_child(level4)
			current_level = level4
			player_spawn = level4.get_node("PlayerSpawn").global_transform.origin
	if dlight and current_level.get_node("DirectionalLight") != null:
		dlight.free()

func free_level(index):
	match index:
		1:
			if level1:
				level1.queue_free()
		2:
			if level2:
				level2.queue_free()
		3:
			if level3:
				level3.queue_free()

func game_over():
	if reset_timer == -INF:
		hud.fade_to_black(0.2,1,0.5)
		reset_timer = 0.2

func reset_level():
	get_tree().call_group("Robot", "reset")
	if player.last_respawn != null:
		player.translation = player.last_respawn
	else:
		player.translation = player_spawn
	#todo reset level




