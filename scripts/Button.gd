extends Spatial

signal fully_pressed
signal fully_released
signal partially_pressed(amount)

var pressing := false
var press_timer := 0.0

func _ready():
	set_process(true)

func _process(dt):
	var bodies = $ButtonRigidBody/Area.get_overlapping_bodies()
	pressing = false
	for b in bodies:
		if b.is_in_group("Player") or b.is_in_group("Heavy"):
			pressing = true
			break
	
	if press_timer > 0.99:
		emit_signal("fully_pressed")
	elif press_timer < 0.1:
		emit_signal("fully_released")
	else:
		emit_signal("partially_pressed", press_timer)
	if pressing:
		press_timer += dt
	else:
		press_timer -= dt * 2
	press_timer = clamp(press_timer, 0.0, 1.0)
	$ButtonRigidBody.translation.y = lerp(0.19, -0.1, press_timer)

func _on_body_entered(body):
	print("Benis")