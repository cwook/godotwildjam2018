extends Area

export var dontleave_mode = false
export var tp_dontfalls = true
export var tp_player = false

var timer_fadeout = 0.0

func _ready():
	set_process(false)

func _on_DontFallArea_body_entered(body):
	if body.is_in_group("Player") and tp_player and !dontleave_mode:
		Game.player.last_respawn = $Respawn.global_transform.origin
		Game.game_over()
	if body.is_in_group("DontFall") and tp_dontfalls and !dontleave_mode:
		body.global_transform.origin = $Respawn.global_transform.origin
		if body is RigidBody:
			body.linear_velocity = Vector3(0,0,0)
			body.mode = RigidBody.MODE_RIGID
			body.sleeping = false
		elif body is KinematicBody:
			body.velocity = Vector3()


func _on_DontFallArea_body_exited(body):
	if body.is_in_group("DontFall") and dontleave_mode:
		#print(body)
		if Game.player.current_pickup == body:
			return
		body.global_transform.origin = $Respawn.global_transform.origin
		print(body.global_transform.origin == $Respawn.global_transform.origin)
		if body is RigidBody:
			body.linear_velocity = Vector3(0,0,0)
			body.mode = RigidBody.MODE_RIGID
			body.sleeping = false
		elif body is KinematicBody:
			body.velocity = Vector3()