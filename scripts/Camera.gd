extends Spatial

enum MODE {FIXED_FOLLOW, FIXED_LOOK}

var mode = MODE.FIXED_FOLLOW
onready var cam = $Camera
onready var target = Game.player
var fl_position := Vector3()
var ff_position : Vector3
var ff_rotation : Quat
var ff_desired_y := 0.0
var ff_offset := 0.0
var ff_offset_desired := 0.0
var camera_adjust_timer := 0.0

var controller_x_speed = 0.01
var mouse_x_sens := 0.001

func _ready():
	target = Game.player
	ff_rotation = Quat(self.rotation.x, self.rotation.y, self.rotation.z, 1.0)
	ff_position = cam.translation
	set_process(true)
	set_physics_process(true)
	set_process_input(true)

func _input(ev):
	if ev is InputEventMouseMotion:
		if mode == MODE.FIXED_FOLLOW:
			ff_desired_y = clamp(ff_desired_y - mouse_x_sens * ev.relative.x, deg2rad(-20), deg2rad(20))
			camera_adjust_timer = 2.0

func _process(dt):
	if mode == MODE.FIXED_FOLLOW:
		var cam_x = Input.get_action_strength("cam_left") - Input.get_action_strength("cam_right")
		ff_desired_y = clamp(ff_desired_y + cam_x * controller_x_speed, deg2rad(-15), deg2rad(15))
		if abs(cam_x) > 0:
			camera_adjust_timer = 2.0

func _physics_process(dt):
	match mode:
		MODE.FIXED_FOLLOW:
			_fixed_follow(dt)
		MODE.FIXED_LOOK:
			_fixed_look(dt)

func _fixed_follow(dt):
	camera_adjust_timer -= dt
	if camera_adjust_timer < 0:
		ff_offset_desired = 0
		if Game.player.velocity.x > 0:
			ff_desired_y = lerp(ff_desired_y, deg2rad(-15), dt*5)
			ff_rotation = Quat(0,ff_desired_y,0,1).normalized()
			ff_offset_desired = 7
		elif Game.player.velocity.x < 0:
			ff_desired_y = lerp(ff_desired_y, deg2rad(15), dt*5)
			ff_rotation = Quat(0,ff_desired_y,0,1).normalized()
			ff_offset_desired = -7
		if abs(Game.player.velocity.x) > 0:
			var this_rotation := Quat(self.transform.basis).normalized().slerp(ff_rotation,dt/4)
			self.transform = Transform(Basis(this_rotation), self.transform.origin)
			ff_offset = lerp(ff_offset, ff_offset_desired, dt/4)
		ff_offset = lerp(ff_offset, ff_offset_desired, dt/8)
	else:
		ff_rotation = Quat(0,ff_desired_y,0,1).normalized()
		ff_offset_desired = range_lerp(ff_desired_y, deg2rad(-15), deg2rad(15), 3, -3)
		var this_rotation := Quat(self.transform.basis).normalized().slerp(ff_rotation,dt*8)
		self.transform = Transform(Basis(this_rotation), self.transform.origin)
		ff_offset = lerp(ff_offset, ff_offset_desired, dt)
	
	self.translation = self.translation.linear_interpolate(target.translation + Vector3(0,1,0) + Vector3(ff_offset,0,0), dt*5)
	self.cam.translation = self.cam.translation.linear_interpolate(ff_position, dt*12)
	self.cam.look_at(self.translation, Vector3(0,1,0))

func _fixed_look(dt):
	self.cam.global_transform.origin = self.cam.global_transform.origin.linear_interpolate(fl_position, dt*12)
	self.cam.look_at(target.translation + Vector3(0,1.5,0), Vector3(0,1,0))


func _set_fixed_look(new_position : Vector3):
	mode = MODE.FIXED_LOOK
	fl_position = new_position

func _set_fixed_follow():
	mode = MODE.FIXED_FOLLOW