extends AudioStreamPlayer

var outside := 0.65
var inside := 0.5
var off := 0.0
var desired : float
var val : float

var outside_cutoff := 14000
var inside_cutoff := 1000
var off_cutoff := 1
var desired_cutoff : float

var lowpass : AudioEffectLowPassFilter

func _ready():
	lowpass = AudioServer.get_bus_effect(3, 0)
	desired = outside
	val = desired
	desired_cutoff = outside_cutoff

func _process(dt):
	lowpass.cutoff_hz = lerp(lowpass.cutoff_hz, desired_cutoff, dt*3)
	
	val = lerp(val, desired, dt/3)
	self.volume_db = 12 * log(val)

func _set_indoor():
	desired = inside
	desired_cutoff = inside_cutoff
	set_process(true)

func _set_outdoor():
		desired = outside
		desired_cutoff = outside_cutoff
		set_process(true)

func _set_off():
		desired = off
		desired_cutoff = off_cutoff
		set_process(true)


